<?
//---Thunder-Indices
//---Sjoerd Huininga - 2011
//---Source: github.com/sjoerdhuininga/Thunder-indices
//---Terms of use: You can freely use and/or modify the code entirely or parts of it
//---however you will have to notify me (sjoerd@huininga.net) of the project you're working on
//---and you'll have to leave this notice untouched. Especially the github link (github.com/sjoerdhuininga/Thunder-indices)

$cachetag=strip_tags($_GET['cache']);
if($cachetag+60 < time()){
	header("Location: showcase.php?cache=".time());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>onweerstaanbaar.com - Local indices</title>
<style type="text/css"> 
<!-- 
body  {
	font: 100% Verdana, Arial, Helvetica, sans-serif;
	background: #666666;
	margin: 0; /* it's good practice to zero the margin and padding of the body element to account for differing browser defaults */
	padding: 0;
	text-align: center; /* this centers the container in IE 5* browsers. The text is then set to the left aligned default in the #container selector */
	color: #000000;
}

/* Tips for Elastic layouts 
1. Since the elastic layouts overall sizing is based on the user's default fonts size, they are more unpredictable. Used correctly, they are also more accessible for those that need larger fonts size since the line length remains proportionate.
2. Sizing of divs in this layout are based on the 100% font size in the body element. If you decrease the text size overall by using a font-size: 80% on the body element or the #container, remember that the entire layout will downsize proportionately. You may want to increase the widths of the various divs to compensate for this.
3. If font sizing is changed in differing amounts on each div instead of on the overall design (ie: #sidebar1 is given a 70% font size and #mainContent is given an 85% font size), this will proportionately change each of the divs overall size. You may want to adjust these divs based on your final font sizing.
*/
.twoColElsLt #container { 
	width: 58em;  /* this width will create a container that will fit in an 800px browser window if text is left at browser default font sizes */
	background: #FFFFFF;
	margin: 0 auto; /* the auto margins (in conjunction with a width) center the page */
	border: 1px solid #000000;
	text-align: left; /* this overrides the text-align: center on the body element. */
} 

/* Tips for sidebar1:
1. Be aware that if you set a font-size value on this div, the overall width of the div will be adjusted accordingly.
2. Since we are working in ems, it's best not to use padding on the sidebar itself. It will be added to the width for standards compliant browsers creating an unknown actual width. 
3. Space between the side of the div and the elements within it can be created by placing a left and right margin on those elements as seen in the ".twoColElsLt #sidebar1 p" rule.
*/
.twoColElsLt #sidebar1 {
	float: left; 
	width: 12em; /* since this element is floated, a width must be given */
	background: #EBEBEB; /* the background color will be displayed for the length of the content in the column, but no further */
	padding: 15px 0; /* top and bottom padding create visual space within this div */
}
.twoColElsLt #sidebar1 h3, .twoColElsLt #sidebar1 p {
	margin-left: 10px; /* the left and right margin should be given to every element that will be placed in the side columns */
	margin-right: 10px;
}

/* Tips for mainContent:
1. If you give this #mainContent div a font-size value different than the #sidebar1 div, the margins of the #mainContent div will be based on its font-size and the width of the #sidebar1 div will be based on its font-size. You may wish to adjust the values of these divs.
2. The space between the mainContent and sidebar1 is created with the left margin on the mainContent div.  No matter how much content the sidebar1 div contains, the column space will remain. You can remove this left margin if you want the #mainContent div's text to fill the #sidebar1 space when the content in #sidebar1 ends.
3. To avoid float drop, you may need to test to determine the approximate maximum image/element size since this layout is based on the user's font sizing combined with the values you set. However, if the user has their browser font size set lower than normal, less space will be available in the #mainContent div than you may see on testing.
4. In the Internet Explorer Conditional Comment below, the zoom property is used to give the mainContent "hasLayout." This avoids several IE-specific bugs that may occur.
*/
.twoColElsLt #mainContent {
 	margin: 0 1.5em 0 13em; /* the right margin can be given in ems or pixels. It creates the space down the right side of the page. */
} 

/* Miscellaneous classes for reuse */
.fltrt { /* this class can be used to float an element right in your page. The floated element must precede the element it should be next to on the page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* this class can be used to float an element left in your page */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* this class should be placed on a div or break element and should be the final element before the close of a container that should fully contain a float */
	clear:both;
    height:0;
    font-size: 1px;
    line-height: 0px;
}
--> 
</style><!--[if IE]>
<style type="text/css"> 
/* place css fixes for all versions of IE in this conditional comment */
.twoColElsLt #sidebar1 { padding-top: 30px; }
.twoColElsLt #mainContent { zoom: 1; padding-top: 15px; }
/* the above proprietary zoom property gives IE the hasLayout it needs to avoid several bugs */
</style>
<![endif]-->

</head>

<body class="twoColElsLt">

<div id="container">
  <div id="sidebar1">
    <h3>Select your city</h3>
    <p><a href="local.php"'>Custom spot<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Valkenburg"'>Valkenburg<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Schiphol"'>Schiphol<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Vlieland"'>Vlieland<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Berkhout"'>Berkhout<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Terschelling"'>Terschelling<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=WijkaanZee"'>Wijk aan Zee<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=DeBilt"'>De Bilt<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Stavoren"'>Stavoren<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Lelystad"'>Lelystad<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Leeuwarden"'>Leeuwarden<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Marknesse"'>Marknesse<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Deelen"'>Deelen<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Lauwersoog"'>Lauwersoog<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Heino"'>Heino<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Hoogeveen"'>Hoogeveen<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Eelde"'>Eelde<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Hupsel"'>Hupsel<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=NieuwBeerta"'>Nieuw Beerta<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Twenthe"'>Twenthe<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Vlissingen"'>Vlissingen<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Westdorpe"'>Westdorpe<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Wilhelminadorp"'>Wilhelminadorp<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=HvH"'>Hoek van Holland<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Woensdrecht"'>Woensdrecht<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Rotterdam"'>Rotterdam<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Cabauw"'>Cabauw<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=GZ"'>Gilze Rijen<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Herwijnen"'>Herwijnen<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Eindhoven"'>Eindhoven<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Volkel"'>Volkel<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Ell"'>Ell<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Maastricht"'>Maastricht<br /></a>
      <a href="#" onclick='document.but.src="model.php?style=index&loc=Arcen"'>Arcen</a>
      <!-- end #sidebar1 -->
    </p>
    <p><?=$visits?> visits</p>
    </div>
  <div id="mainContent">
    <h1>Local indices</h1>
    <p><img name="but">&nbsp;</p>
    <p>&nbsp;</p>
    <!-- end #mainContent --></div>
	<!-- This clearing element should immediately follow the #mainContent div in order to force the #container div to contain all child floats --><br class="clearfloat" />
<!-- end #container --></div>
</body>
</html>
