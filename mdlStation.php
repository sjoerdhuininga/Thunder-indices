<?
//---Thunder-Indices
//---Sjoerd Huininga - 2011
//---Source: github.com/sjoerdhuininga/Thunder-indices
//---Terms of use: You can freely use and/or modify the code entirely or parts of it
//---however you will have to notify me (sjoerd@huininga.net) of the project you're working on
//---and you'll have to leave this notice untouched. Especially the github link (github.com/sjoerdhuininga/Thunder-indices)

$aanvang = microtime();

for ($K=0; $K<33; $K++){
	$arrMap[$K]->c = 255;
}

$arrMap[0]->id = "Schiphol";	 $arrMap[0]->name = "Schiphol";		$arrMap[0]->x = 224; $arrMap[0]->y = 325; //Schiphol
$arrMap[1]->id = "DeBilt";		 $arrMap[1]->name = "De Bilt";		$arrMap[1]->x = 280; $arrMap[1]->y = 395; //DeBilt
$arrMap[2]->id = "Rotterdam"; 	 $arrMap[2]->name = "Rotterdam";	$arrMap[2]->x = 170; $arrMap[2]->y = 435; //Rotterdam
$arrMap[3]->id = "Lauwersoog";	 $arrMap[3]->name = "Lauwersoog";	$arrMap[3]->x = 458; $arrMap[3]->y = 59;  //Lauwersoog
$arrMap[4]->id = "Maastricht";	 $arrMap[4]->name = "Maastricht AP";$arrMap[4]->x = 384; $arrMap[4]->y = 709; //Maastricht

$arrMap[5]->id = "Terschelling"; $arrMap[5]->name = "Terschelling";	$arrMap[5]->x = 302; $arrMap[5]->y = 58;  //Terschelling
$arrMap[6]->id = "Vlissingen";	 $arrMap[6]->name = "Vlissingen";	$arrMap[6]->x = 44;  $arrMap[6]->y = 555; //Vlissingen
$arrMap[7]->id = "Lelystad";	 $arrMap[7]->name = "Lelystad";		$arrMap[7]->x = 364; $arrMap[7]->y = 286; //Lelystad
$arrMap[8]->id = "Eindhoven";	 $arrMap[8]->name = "Eindhoven";	$arrMap[8]->x = 350; $arrMap[8]->y = 565; //Eindhoven
$arrMap[9]->id = "Deelen";		 $arrMap[9]->name = "Deelen";		$arrMap[9]->x = 389; $arrMap[9]->y = 387; //Deelen

$arrMap[10]->id = "Leeuwarden";	 $arrMap[10]->name = "Leeuwarden";	$arrMap[10]->x = 391; $arrMap[10]->y = 91;  //Leeuwarden
$arrMap[11]->id = "Hoogeveen";	 $arrMap[11]->name = "Hoogeveen";	$arrMap[11]->x = 471; $arrMap[11]->y = 228; //Hoogeveen
$arrMap[12]->id = "Heino"; 		 $arrMap[12]->name = "Heino";		$arrMap[12]->x = 443; $arrMap[12]->y = 279; //Heino
$arrMap[13]->id = "Arcen";		 $arrMap[13]->name = "Arcen";		$arrMap[13]->x = 451; $arrMap[13]->y = 540; //Arcen
$arrMap[14]->id = "Ell";		 $arrMap[14]->name = "Ell";			$arrMap[14]->x = 388; $arrMap[14]->y = 619; //Ell

$arrMap[15]->id = "Marknesse";	 $arrMap[15]->name = "Marknesse";	$arrMap[15]->x = 391; $arrMap[15]->y = 241; //Marknesse
$arrMap[16]->id = "Valkenburg";	 $arrMap[16]->name = "Valkenburg";	$arrMap[16]->x = 167; $arrMap[16]->y = 376; //Valkenburg
$arrMap[17]->id = "Vlieland";	 $arrMap[17]->name = "Vlieland";	$arrMap[17]->x = 254; $arrMap[17]->y = 95;  //Vlieland
$arrMap[18]->id = "DenHelder";	 $arrMap[18]->name = "Den Helder";	$arrMap[18]->x = 226; $arrMap[18]->y = 175; //DenHelder
$arrMap[19]->id = "WijkaanZee";	 $arrMap[19]->name = "Wijk aan Zee";$arrMap[19]->x = 205; $arrMap[19]->y = 295; //WijkaanZee

$arrMap[20]->id = "Stavoren";	 $arrMap[20]->name = "Stavoren";	$arrMap[20]->x = 327; $arrMap[20]->y = 189; //Stavoren
$arrMap[21]->id = "NieuwBeerta"; $arrMap[21]->name = "Nieuw Beerta";$arrMap[21]->x = 594; $arrMap[21]->y = 109; //NieuwBeerta
$arrMap[22]->id = "Eelde";		 $arrMap[22]->name = "Eelde";		$arrMap[22]->x = 508; $arrMap[22]->y = 132; //Eelde
$arrMap[23]->id = "Twenthe";	 $arrMap[23]->name = "Twenthe";		$arrMap[23]->x = 557; $arrMap[23]->y = 356; //Twenthe
$arrMap[24]->id = "Hupsel";		 $arrMap[24]->name = "Hupsel";		$arrMap[24]->x = 527; $arrMap[24]->y = 404; //Hupsel

$arrMap[25]->id = "Herwijnen";	 $arrMap[25]->name = "Herwijnen";	$arrMap[25]->x = 273; $arrMap[25]->y = 462; //Herwijnen
$arrMap[26]->id = "Cabauw"; 	 $arrMap[26]->name = "Cabauw";		$arrMap[26]->x = 240; $arrMap[26]->y = 443; //Cabauw
$arrMap[27]->id = "Berkhout";	 $arrMap[27]->name = "Berkhout";	$arrMap[27]->x = 262; $arrMap[27]->y = 256; //Berkhout
$arrMap[28]->id = "HvH";		 $arrMap[28]->name = "Hoek van Holland";	$arrMap[28]->x = 116; $arrMap[28]->y = 429; //HvH
$arrMap[29]->id = "Wilhelminadorp"; $arrMap[29]->name = "Wilhelminadorp";	$arrMap[29]->x = 72;  $arrMap[29]->y = 539; //Wilhelminadorp

$arrMap[30]->id = "Woensdrecht"; $arrMap[30]->name = "Woensdrecht";	$arrMap[30]->x = 149; $arrMap[30]->y = 563; //Woensdrecht
$arrMap[31]->id = "GZ";			 $arrMap[31]->name = "Gilze Rijen";	$arrMap[31]->x = 246; $arrMap[31]->y = 536; //GZ
$arrMap[32]->id = "Volkel";		 $arrMap[32]->name = "Volkel";		$arrMap[32]->x = 373; $arrMap[32]->y = 513; //Volkel
$arrMap[33]->id = "Westdorpe";	 $arrMap[33]->name = "Westdorpe";	$arrMap[33]->x = 84;  $arrMap[33]->y = 611; //Westdorpe

$afloop = microtime();

//die((($afloop-$aanvang)*1000)." ms");
?>