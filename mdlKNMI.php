<?
//---Thunder-Indices
//---Sjoerd Huininga - 2011
//---Source: github.com/sjoerdhuininga/Thunder-indices
//---Terms of use: You can freely use and/or modify the code entirely or parts of it
//---however you will have to notify me (sjoerd@huininga.net) of the project you're working on
//---and you'll have to leave this notice untouched. Especially the github link (github.com/sjoerdhuininga/Thunder-indices)

global $arrMap;

//------------------
//KNMI TROEP INLADEN
//------------------

$filename = "http://www.knmi.nl/actueel/";
$handle = fopen($filename, "r");
$contents = stream_get_contents($handle);
fclose($handle);

for ($K=0; $K<33; $K++){

	$StationName 	= $arrMap[$K]->name;
	
	$tp1 = explode("<td>$StationName</td>", $contents);
	$tp2 = explode("<td align=right>", $tp1[1]);
	$tp3 = explode("&nbsp;</td>", $tp2[1]);
	$tp4 = $tp3[0];

	$tpd3 = explode("&nbsp;</td>", $tp2[2]);
	$tdp4 = $tp2[2];
	$tdp5 = explode(" ",$tp2[6]);
	$tdp6 = $tdp5[0];
	$tdp7 = explode("&nbsp;",$tp2[3]);
	$tdp8 = $tdp7[0];

	if(!is_numeric($tp4)){
		$tp4=255;
	}
	$arrMap[$K]->c 	= $tp4;
	$arrMap[$K]->td = Dewp($tp4, $tdp4);
	$arrMap[$K]->p	= $tdp6;
	$arrMap[$K]->wd = trim($tdp8);	
	
	$statT = $StationName."T";
	$statTd = $StationName."Td";
	$statP = $StationName."P";
	
	$$statT = $tp4;
	$$statTd = Dewp($tp4, $tdp4);
	$$statP = $tdp6;
}

function Dewp($T, $RH){
	$a = Log($RH / 100) / Log(2.718282) + (17.62 * $T / (243.12 + $T));
	$Dewp = 243.12 * $a / (17.62 - $a);
	return $Dewp;
}
?>