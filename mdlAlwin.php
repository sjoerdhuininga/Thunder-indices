<?
//---Thunder-Indices
//---Sjoerd Huininga - 2011
//---Source: github.com/sjoerdhuininga/Thunder-indices
//---Terms of use: You can freely use and/or modify the code entirely or parts of it
//---however you will have to notify me (sjoerd@huininga.net) of the project you're working on
//---and you'll have to leave this notice untouched. Especially the github link (github.com/sjoerdhuininga/Thunder-indices)

//----------
//mdlAlwin.php - External code
//Copyright: Alwin Haklander, proefschrift
//----------
$Kelvin	= 273.15;
$Rd		= 287.04;
$Rv		= 461.5;
$eps	= $Rd / $Rv;
$cpd	= 1005.7;
$cvv	= 1410;
$cpv	= 1870;
$lv0	= 2501000;
$g		= 9.80616;
$omega	= 0.00007292;

function GetEs($sTact){
global $Kelvin;
	$TempK = $sTact + $Kelvin;
	if ($TempK > 0){
		$power = 53.67957 - 6743.769 / $TempK - 4.851 * Log($TempK);
		$Es = Exp($power);
	}
		return($Es);
}

function GetMixingRatio($e, $sp){
global $eps;
	$mix = -999;
	if ($e >= 0 && $sp != $e && $sp > -998){
	//echo("e $e, eps $eps, p $sp,".($eps * $e));
		@$mix = $eps * $e / ($sp - $e);
		
	} 
	return $mix;
}

function Get_tLCL($sTact, $sTd){
global $Kelvin;
	$tLCL = -999;
	if ($sTact > -998 && $sTd > -998){
		$TempK 	= $sTact + $Kelvin;
		$e		= GetEs($sTd);
		$noemer = 3.5 * Log($TempK) - Log($e) - 4.805;
		$tLCL	= 2840 / $noemer + 55 - $Kelvin;
	}
	return $tLCL;
}

function Get_pLCL($Tact, $Td, $p){
global $Kelvin, $Rd, $eps, $cpd, $cpv;
	$pLCL = -999;
	if ($Tact > -998 && $Td > -998 && $p > -998){
		$tLCL  = Get_tLCL($Tact, $Td);
		$tLCLK = $tLCL + $Kelvin;
		$TempK = $Tact + $Kelvin;
		$e	   = GetEs($Td);
		$mix   = GetMixingRatio($e, $p);
		$power = ($cpd + $mix * $cpv) / ($Rd * (1 + $mix / $eps));
		$pLCL  = $p * pow(($tLCLK / $TempK), $power);
	}
	return $pLCL;
}

function Liftdry($startT, $startP, $endP){
global $Kelvin, $Rd, $cpd; 
	$Temp = -999;
	if ($startT > -998 && $startP > -998 && $endP > -998){
		$Theta = GetTheta($startT, $startP);
		$Temp = $Theta / pow((1000 / $endP), ($Rd / $cpd)) - $Kelvin;
	}
	return $Temp;
}

function LiftWet($startT, $startP, $endP){
	$Temp = -999; $Pres = -999; $Delp = -999;
	if($startT > -998 && $startP > -998 & $endP > -998){
		$Temp = $startT;
		$Pres = $startP;
		$Delp = 1;
		do {
			$Temp = $Temp - $Delp * GetGammaW($Temp, $Temp, $Pres - $Delp / 2);
			$Pres = $Pres - $Delp;
		} while($Pres > $endP);
	}
	$Liftwet = $Temp;
	return $Liftwet;
}

function GetGammaW($TempC, $DewpC, $Pres){
global $Kelvin, $g, $cpd, $Rd, $cpv, $eps, $Rv;
	$dtdp = -999;
	if ($TempC > -998 && $DewpC > -998 && $Pres > -998){
		$TempK = $TempC + $Kelvin;
		$DewpK = $DewpC + $Kelvin;
		$e = GetEs($DewpC);
		$mix = GetMixingRatio($e, $Pres);
		$tv = GetTv_mixingratio($TempC, $mix);
		$lv = GetLv($TempC);
		$term_a = ($g / $cpd) * (1 + $mix) / (1 + $mix * ($cpv/$cpd));
		$term_b = 1 + ($lv * $mix) / ($Rd * $TempK);
		$term_c = ($lv * $lv) * $mix * (1 + $mix / $eps);
		$term_d = $Rv * ($TempK * $TempK) * ($cpd + $mix * $cpv);
		$dtdzmin = $term_a * ($term_b / (1 + $term_c / $term_d));
		$dtdp = $dtdzmin * ($Rd * $tv) / ($Pres * $g); //grad C / hPa
		//echo "e $e, mix $mix, tv $tv, lv $lv";
	}
	$GetGammaW = $dtdp;
	return ($GetGammaW);
}

function GetLv($TempC){
global $lv0;
	if($TempC > -998){
		$GetLv = $lv0 - 2730 * $TempC;
	}
	return $GetLv;
}

function GetTv_mixingratio($TempC, $mix){
global $Kelvin, $eps;
	$TempV = -999;
	if ($TempC > -998 && $mix > -998){
		$TempK = $TempC + $Kelvin;
		$TempV = $TempK * (1 + $mix / $eps) / (1 + $mix);
	}
	return($TempV);
}

function GetTheta($Tact, $Pres){
global $Kelvin, $Rd, $cpd; 
	$Theta = -999;
	If ($Tact > -998 && $Pres > -998){
		$Theta = ($Tact + $Kelvin) * pow((1000 / $Pres), ($Rd/ $cpd));
	}
	return $Theta;
}

function get_CAPE($Tat, $sTd){
global $P, $Tact, $Z;
	$pLCL = get_pLCL($Tat, $sTd, $P[0]);
	$pLFC = get_LFC($Tat, $sTd, $pLCL);
	$tmpCAPE = 0;
	for($i=1; $i<count($P); $i++){
		$dZ = $Z[$i] - $Z[$i -1];
		$Tp = LiftWet(LiftDry($Tat, $P[0], $pLCL), $pLCL, $P[$i]);
		$Te = $Tact[$i];
		//echo "te $Te . tp $Tp <br>";
		if ($Tp > $Te){
			@$tmpCAPE = $tmpCAPE + (((GetTv_mixingratio($Tp, GetMixingRatio(GetEs($Tp), $P[$i])) - GetTv_mixingratio($Te, GetMixingRatio(GetEs($Te), $P[$i]))) / GetTv_mixingratio($Te, GetMixingRatio(GetEs($Te), $P[$i]))) * $dZ);
		}
	}
	$get_CAPE = $tmpCAPE * 9.81;
	return $get_CAPE;
}


function get_LFC($Tat, $Td, $pLCL){
global $P;
	for($i=1; $i < count($P); $i++){
		if($pLCL > $P[$i]){
			$dZ = $Z[$i] - $Z[$i-1];
			$Tp = LiftWet(LiftDry($Tat, $P[0], $pLCL), $pLCL, $P[$i]);
			$Te = $Tact[$i];
			if($Tp > $Te && $Tp < $Tact[$i -1]){
				return ($P[$i]);
			}
		}
	}

}

function TempColor($Tat, $Tdat){
global $style;

	if($style=="conv"){
		global $Tcon;
		if($Tat > $Tcon){
			$Tempcolor = RGB(255,88,26);	
		}else{
			$Tempcolor = RGB(200,255,200);	
		}
	}elseif($style=="tact"){
	$Tempcolor = RGB(0,0,0);
	$Tat = round($Tat,0);
	if($Tat == NULL){$Tat = -1;}
		switch (round($Tat)){
		case ($Tat > -40 && $Tat < -31): 
			$Tempcolor = RGB(($Tat + 40) * 20, 0, ($Tat + 40) * 20);
			return $Tempcolor;
		case ($Tat > -30 && $Tat < -21):
			$Tempcolor = RGB(200 - ($Tat + 30) * 20,0, 200 - ($Tat + 30) * 10);
			return $Tempcolor;
		case ($Tat > -20 && $Tat < -9):
			$Tempcolor = RGB(0, ($Tat + 20) * 20, 100 + ($Tat + 20) * 10);
			return $Tempcolor;
		case ($Tat > -10 && $Tat < 1):
			$Tempcolor = RGB(0,200,250- ($Tat + 10) * 20);
			return $Tempcolor;
		case ($Tat > 0 && $Tat < 11):
			$Tempcolor = RGB($Tat * 20,200,0);
			return $Tempcolor;
		case ($Tat > 10 && $Tat < 21):
			$Tempcolor = RGB(200,200 - ($Tat -10) * 10,0);
			return $Tempcolor;
		case ($Tat > 20 && $Tat < 31):
			$Tempcolor = RGB(200 - ($Tat - 20) * 10, 100 - ($Tat - 20) * 10 ,0);
			return $Tempcolor;
		case ($Tat > 30 && $Tat < 41):
			$Tempcolor = RGB(100 - ($Tat - 30) * 10,0,0);
			return $Tempcolor;
		}
}elseif($style=="td"){
	$Tempcolor = RGB(0,0,0);
	$Tat = round($Tdat,0);
		switch (round($Tat)){
		case ($Tat > -40 && $Tat < -31): 
			$Tempcolor = RGB(($Tat + 40) * 20, 0, ($Tat + 40) * 20);
			return $Tempcolor;
		case ($Tat > -30 && $Tat < -21):
			$Tempcolor = RGB(200 - ($Tat + 30) * 20,0, 200 - ($Tat + 30) * 10);
			return $Tempcolor;
		case ($Tat > -20 && $Tat < -11):
			$Tempcolor = RGB(0, ($Tat + 20) * 20, 100 + ($Tat + 20) * 10);
			return $Tempcolor;
		case ($Tat > -10 && $Tat < 1):
			$Tempcolor = RGB(0,200,200- ($Tat + 10) * 20);
			return $Tempcolor;
		case ($Tat > 0 && $Tat < 11):
			$Tempcolor = RGB($Tat * 20,200,0);
			return $Tempcolor;
		case ($Tat > 10 && $Tat < 21):
			$Tempcolor = RGB(200,200 - ($Tat -10) * 10,0);
			return $Tempcolor;
		case ($Tat > 20 && $Tat < 31):
			$Tempcolor = RGB(200 - ($Tat - 20) * 10, 100 - ($Tat - 20) * 10 ,0);
			return $Tempcolor;
		case ($Tat > 30 && $Tat < 41):
			$Tempcolor = RGB(100 - ($Tat - 30) * 10,0,0);
			return $Tempcolor;
		}
		}elseif($style=="cape"){
	
	//if ($arrCape($Tat + 40, $Tdat + 40)){
	//	$CAPE1 = arrCape($Tat + 40, $Tdat + 40);
	//}else{
		//$CAPE1 = get_CAPE($Tat, $Tdat);
	//	$CAPE1 = arrCape($Tat + 40, $Tdat + 40);		
	//}
		$Tat=round($Tat/1000);
		switch ($Tat){
		case ($Tat > 0 && $Tat < 2): 
			$Tempcolor = RGB(255,222,122);
			return $Tempcolor;
		case ($Tat > 3 && $Tat < 4):
			$Tempcolor = RGB(255,204,70);
			return $Tempcolor;
		case ($Tat > 5 && $Tat < 6):
			$Tempcolor = RGB(255,163,39);
			return $Tempcolor;
		case ($Tat > 7 && $Tat < 8):
			$Tempcolor = RGB(255,127,28);
			return $Tempcolor;
		case ($Tat > 9 && $Tat < 10):
			$Tempcolor = RGB(255,88,26);
			return $Tempcolor;
		case ($Tat > 11 && $Tat < 15):
			$Tempcolor = RGB(255,48,30);
			return $Tempcolor;
		case ($Tat > 16 && $Tat < 20):
			$Tempcolor = RGB(255,33,111);
			return $Tempcolor;
		case ($Tat > 21 && $Tat < 25):
			$Tempcolor = RGB(255,28,187);
			return $Tempcolor;
		}		
	
	}
	return $Tempcolor;
}

function RGB($R, $G, $B){
global $im2;
	$a = imagecolorallocate($im2, $R, $G, $B);
	return $a;
}

function Distance($Array1, $Array2){
//die("$Array1[1] $Array1[0] + $Array2[1] $Array2[0]");
	$Distance= (($Array2[0]-$Array1[0]) * ($Array2[0] - $Array1[0]) + ($Array2[1] - $Array1[1]) * ($Array2[1] - $Array1[1]));
	return $Distance;
}

?>