<?
//---Thunder-Indices
//---Sjoerd Huininga - 2011
//---Source: github.com/sjoerdhuininga/Thunder-indices
//---Terms of use: You can freely use and/or modify the code entirely or parts of it
//---however you will have to notify me (sjoerd@huininga.net) of the project you're working on
//---and you'll have to leave this notice untouched. Especially the github link (github.com/sjoerdhuininga/Thunder-indices)

set_time_limit (180);
//Import modules
include_once("mdlAlwin.php");
include_once("mdlStation.php");
include_once("mdlKNMI.php");

$style = strip_tags($_GET['style']);
if(!$style){$style="conv";}

$timestring = date('H:i d M Y');
$starttime = time();

//------------------------
//SOUNDING DATA VERKRIJGEN
//------------------------

date('H:i d M Y');
$jaar=date('Y');
$maand=date('m');
if((date('H') - 2) >= 12){
	$runz = '12';
}elseif(date('H') - 4 < 0){
	$runz = '12';
}else{
	$runz = '00';
}
$run1=date('d').$runz;
$station='06260';

$filename = "http://129.72.27.74/cgi-bin/sounding?region=europe&TYPE=TEXT:LIST&YEAR=$jaar&MONTH=$maand&FROM=$run1&TO=$run1&STNM=$station";

$handle = fopen($filename, "r");
$contents = stream_get_contents($handle);
fclose($handle);

$contents = str_replace("      ", "|" ,$contents);
$contents = str_replace("     ", "|" ,$contents);
$contents = str_replace("    ", "|" ,$contents);
$contents = str_replace("   ", "|" ,$contents);
$contents = str_replace("  ", "|" ,$contents);
$contents = str_replace(" ", "|" ,$contents);

$matrix = explode("\n", $contents);
$j=-1;
for ($i=9; $i<count($matrix); $i++){
	$mat = explode("|", $matrix[$i]);
	if($mat[1] <>'' && $mat[2] <>'' && $mat[3] <>''){
		$j++;
     	$P[$j]=$mat[1];
		$Z[$j]=$mat[2];
		$Tact[$j]=$mat[3];
		$Td[$j]=$mat[4];
		$Rh[$j]=$mat[5];
	}
}

for ($i=1; $i<count($P)-3; $i++){
	if($P[$i]==500){
		$T500  = $Tact[$i];
		$Td500 = $Td[$i];	
		$Z500  = $Z[$i];
	}
	if($P[$i]==700){
		$T700  = $Tact[$i];
		$Td700 = $Td[$i];	
		$Z700  = $Z[$i];
	}
	if($P[$i]==850){
		$T850  = $Tact[$i];
		$Td850 = $Td[$i];	
		$Z850  = $Z[$i];
	}	
	if($P[$i]==1000){
		$T1000  = $Tact[$i];
		$Td1000 = $Td[$i];	
		$Z1000  = $Z[$i];
	}	
	if((!$T850) && ($P[$i-1] > 850 && $P[$i] < 850)){
		$T850  = $Tact[$i];
		$Td850 = $Td[$i];	
		$Z850  = $Z[$i];
	}
}

//Locatie specifiek gebeuren
$loc = strip_tags($_GET['loc']);
if($loc){
	if(substr($loc,0,1)=="?"){
		$x = substr($loc,1,3);
		$y = substr($loc,5,3);
		
		$Weegfactor	= -1.5;
		$a=0; $af=0; $b=0; $btd=0;
	     	for ($K=0; $K<33; $K++){
	       		if ($arrMap[$K]->c <> 255){
	       			$a = Distance(Array($x, $y), Array($arrMap[$K]->x, $arrMap[$K]->y)) + 0.000001;
	       			$af = $af + pow($a, $Weegfactor);
	       		}
	       	}
	       	if($x == $arrMap[1]->x && $y == $arrMap[1]->y){
	       		$af= $af;
	       	}
	       	for ($K=0; $K<33; $K++){
	       		if ($arrMap[$K]->c <> 255){
	       			$a = Distance(Array($x, $y), Array($arrMap[$K]->x, $arrMap[$K]->y)) + 0.000001;
	       			$b = $b + ($arrMap[$K]->c * pow($a, ($Weegfactor)) / $af);
	       			$btd = $btd + ($arrMap[$K]->td * pow($a, ($Weegfactor)) / $af);
	       		}
	       	}
	    $Tact[0]=round($b,1);
	    $Td[0]=round($btd,1);   
	    $loc = "Custom location, x:$x y:$y"; 	
	}else{
		$loct=$loc."T";
		$loctd=$loc."Td";
		$locP=$loc."P";
		
		if($$loct>-998){
			$Tact[0]=($$loct);
		}
		if($$loctd>-998){
			$Td[0]=round(($$loctd),1);
		}
		if($$locP>0){
			$P[0]=round($$locP,1);
		}
	}
}


$im2 = imagecreatetruecolor(1,1);

$pLCL 	  = round(Get_pLCL($Tact[0],$Td[0],$P[0]));
$Tcon	  = round(Liftdry($T850, 850, $P[0]),2);

if ($style=="index"){

$TT_index = $T850 + $Td850 - 2 * $T500;
if($TT_index<44){
	$TT_caption = "Geen onweer";
	$TT_color	= RGB(255,222,122);
}elseif($TT_index<50 && $TT_index>43){
	$TT_caption = "Onweer (zeer) waarschijnlijk";
	$TT_color 	= RGB(255, 163, 39);
}elseif($TT_index==51 || $TT_index==52){
	$TT_caption	= "Geisoleerd zwaar onweer mogelijk";
	$TT_color	= RGB(255, 88,26);
}elseif($TT_index>52 || $TT_index<57){
	$TT_caption = "Zwaar onweer op grote schaal";
	$TT_color	= RGB(255, 33,111);
}elseif($TT_index>56){
	$TT_caption = "Zeer zwaar onweer mogelijk";
	$TT_color	= RGB(255,10,255);

}
$K_index  = ($T850 - $T500) + $Td850 - ($T700 - $Td700);
if($K_index<18){
	$K_caption	= "Geen onweer";
	$K_color	= RGB(255,222,122);
}elseif($K_index==18 || $K_index == 19){
	$K_caption	= "Onweer onwaarschijnlijk";
	$K_color	= RGB(255,163,39);
}elseif($K_index > 19 && $K_index < 26){
	$K_caption	= "Geisoleerd onweer mogelijk";
	$K_color	= RGB(255,88,26);
}elseif($K_index > 25 && $K_index < 31){
	$K_caption	= "Grote kans op onweer";
	$K_color	= RGB(255,33,111);
}elseif($K_index > 30 && $K_index < 36){
	$K_caption	= "Hoge onweersactiviteit, kans op vorming MCS";
	$K_color	= RGB(255,10,255);
}elseif($K_index > 35){
	$K_caption	= "Zwaar onweer (mogelijk)";
	$K_color	= RGB(255,22,122);
}

$Boyden	  = ($Z700 - $Z1000) / 10 - $T700 - 200;

if($Boyden<95){
	$Boyden_caption = "Onweer niet waarschijnlijk";
	$Boyden_color	= RGB(255,222,122);
}elseif($Boyden==94){
	$Boyden_caption = "Onweer mogelijk";
	$Boyden_color 	= RGB(255,88,26);
}elseif($Boyden>94){
	$Boyden_caption	= "Grote kans op onweer";
	$Boyden_color	= RGB(255,22,122);
}

$LITd	  = LiftDry($Tact[0],$P[0],$pLCL);
$LITw	  = LiftWet($LITd, $pLCL, 500);
$LI		  = round($T500 - $LITw,2);

if($LI >= 0){
	$LI_caption = "Onweer niet waarschijnlijk";
	$LI_color 	= RGB(255,222,122);
}elseif($LI < 0 && $LI > -2){
	$LI_caption = "Onweer mogelijk";
	$LI_color	= RGB(255,88,26);
}elseif($LI <= -2 && $LI > -6){
	$LI_caption = "Onweer aannemelijk";
	$LI_color	= RGB(255,33,111);
}elseif($LI <= -6){
	$LI_caption	= "Zwaar onweer aanemelijk";
	$LI_color	= RGB(255,10,255);
}

$CAPE = round(get_CAPE($Tact[0], $Td[0]),1);
$CAPE_color = RGB(0,0,0);		
		
$e = 6.11 * exp(5417.7530 * ((1/273.16)-(1/(($Td[0])+273.15))));	
$h = (0.5555)*(e - 10.0);
$Humidex = round($Tact[0] + $h,1);

//----------
//KNMI TROEP
//----------
if($Tact[0] > $Tcon){
	$genforecast = "Convectieve temperatuur is bereikt, cumuli zal snel vormen.";
}

	$style="tact";
		
	$ind = imagecreatetruecolor(700,225);
	$v=$_SERVER["DOCUMENT_ROOT"];	
	$skinfile = "$v/model/bgind.png";

	$fillcolor = imagecolorallocate($ind, 255, 255, 255);

	imagefill ($ind , 0,0 , $fillcolor);
	imagestring($ind, 5, 150, 10, $loc." ".$timestring, $textcolor);	
//Tact
	imagestring($ind, 5, 10, 30, "Tact:", $textcolor);
	imagestring($ind, 5, 300, 30, $Tact[0], TempColor($Tact[0],0));
	imagestring($ind, 5, 400, 30, "[C]", $textcolor);
//Dauwpunt
	imagestring($ind, 5, 10, 45, "Td:", $textcolor);
	imagestring($ind, 5, 300, 45, $Td[0], TempColor($Td[0],0));
	imagestring($ind, 5, 400, 45, "[C]", $textcolor);
//Total Totals	
	imagestring($ind, 5, 10, 60, "Total Totals:", $textcolor);
	imagestring($ind, 5, 300, 60, $TT_index, $TT_color);
	imagestring($ind, 5, 400, 60, $TT_caption, $textcolor);
//K index		
	imagestring($ind, 5, 10, 75, "K index:", $textcolor);
	imagestring($ind, 5, 300, 75, $K_index, $K_color);
	imagestring($ind, 5, 400, 75, $K_caption, $textcolor);
//Convective T		
	imagestring($ind, 5, 10, 90, "Convective Temperature:", $textcolor);
	imagestring($ind, 5, 300, 90, $Tcon, TempColor($Tcon,0));
	imagestring($ind, 5, 400, 90, "[C]", $textcolor);
//Boyden index
	imagestring($ind, 5, 10, 105, "Boyden index:", $textcolor);
	imagestring($ind, 5, 300, 105, $Boyden, $Boyden_color);
	imagestring($ind, 5, 400, 105, $Boyden_caption, $textcolor);
//Lifted index
	imagestring($ind, 5, 10, 120, "Lifted index:", $textcolor);
	imagestring($ind, 5, 300, 120, $LI, $LI_color);
	imagestring($ind, 5, 400, 120, $LI_caption, $textcolor);
//Lifted Condensation Level	
	imagestring($ind, 5, 10, 135, "Lifted Condensation Level:", $textcolor);
	imagestring($ind, 5, 300, 135, $pLCL, $textcolor);
	imagestring($ind, 5, 400, 135, "[hPa]", $textcolor);
//Convective Available Potential Energy
	imagestring($ind, 5, 10, 150, "CAPE:", $textcolor);
	imagestring($ind, 5, 300, 150, $CAPE, $CAPE_color);
	imagestring($ind, 5, 400, 150, "[J/kg] ".$CAPE_caption, $textcolor);
	
	imagestring($ind, 5, 10, 165, "Ground pressure:", $textcolor);
	imagestring($ind, 5, 300, 165, $P[0], $textcolor);
	imagestring($ind, 5, 400, 165, "[hPa]", $textcolor);
//Humidex
	imagestring($ind, 5, 10, 180, "Humidex:", $textcolor);
	imagestring($ind, 5, 300, 180, $Humidex, $textcolor);
	imagestring($ind, 5, 400, 180, "[C]", $textcolor);
		
	imagestring($ind, 5, 10, 195, $genforecast, $textcolor);
	
	imagestring($ind, 10, 10, 205, "Based on sounding ".$runz."Z De Bilt", $textcolor);
	//imagestring($ind, 5, 10, 130, "Level of Free Convection:", $textcolor);
	//imagestring($ind, 5, 300, 130, $LFC." hPa", $textcolor);		
	//imagepng($ind, 'model/modeloutputindex'.$loc.'.png');
	header('Content-type: image/png');
	imagepng($ind);
	die();
}
echo"Modelrun completed in $rntime seconds!\n";
?>